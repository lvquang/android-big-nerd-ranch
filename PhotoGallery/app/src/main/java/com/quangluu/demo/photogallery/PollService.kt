package com.quangluu.demo.photogallery

import android.app.*
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.SystemClock
import android.util.Log
import java.util.concurrent.TimeUnit
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat


class PollService : IntentService(TAG) {
    companion object {
        const val TAG = "PollService"
        const val ACTION_SHOW_NOTIFICATION = "com.quangluu.demo.photogallery.SHOW_NOTIFICATION"
        const val PERM_PRIVATE = "com.quangluu.demo.photogallery.PRIVATE"
        const val REQUEST_CODE = "REQUEST_CODE"
        const val NOTIFICATION = "NOTIFICATION"

        val POLL_INTERVAL_MS = TimeUnit.MINUTES.toMillis(15)

        fun newIntent(context: Context): Intent {
            return Intent(context, PollService::class.java)
        }

        fun setServiceAlarm(ctx: Context, isOn: Boolean) {
            val i = PollService.newIntent(ctx)
            val pi = PendingIntent.getService(ctx, 0, i, 0)

            val alarmManager = ctx.getSystemService(Context.ALARM_SERVICE) as AlarmManager

            if (isOn) {
                alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME,
                        SystemClock.elapsedRealtime(),
                        POLL_INTERVAL_MS,
                        pi)
            } else {
                alarmManager.cancel(pi)
                pi.cancel()
            }

            QueryPreferences.setAlarmOn(ctx, isOn)
        }

        fun isServiceAlarmOn(ctx: Context): Boolean {
            val i = PollService.newIntent(ctx)
            val pi = PendingIntent.getService(ctx, 0, i, PendingIntent.FLAG_NO_CREATE)
            return pi != null
        }
    }

    override fun onHandleIntent(intent: Intent?) {
        if (!isNetworkAvailableAndConnected()) return
        Log.i(TAG, "Received an intetnt ${intent}")

        val query = QueryPreferences.getStoredQuery(this)
        val lastResultId = QueryPreferences.getLastResultId(this)
        val galleryItems: ArrayList<GalleryItem>

        if (query == null) {
            galleryItems = FlickrFetcher().fetchRecentPhotos()
        } else {
            galleryItems = FlickrFetcher().searchPhotos(query)
        }

        if (galleryItems.size == 0) return

        val resultId = galleryItems.first().id

        if (resultId.equals(lastResultId)) {
            Log.i(TAG, "Got an old result ${resultId}")
        } else {
            Log.i(TAG, "Got an new result ${resultId}")

            // create pending intent
            val startPhotoGalleryActivityIntent = Intent(this, PhotoGalleryActivity::class.java)
            val pi = PendingIntent.getActivity(this, 0, startPhotoGalleryActivityIntent, 0)
            val notification = NotificationCompat.Builder(this)
                    .setTicker(resources.getString(R.string.new_picture_title))
                    .setSmallIcon(android.R.drawable.ic_menu_report_image)
                    .setContentTitle(resources.getString(R.string.new_picture_title))
                    .setContentText(resources.getString(R.string.new_picture_text))
                    .setContentIntent(pi)
                    .setAutoCancel(true)
                    .build()

            showBackgroundNotification(0, notification)
        }

        QueryPreferences.setLastResultId(this, resultId)
    }

    private fun showBackgroundNotification(requestCode: Int, notification: Notification) {
        val i = Intent(ACTION_SHOW_NOTIFICATION)
        i.putExtra(REQUEST_CODE, requestCode)
        i.putExtra(NOTIFICATION, notification)
        sendOrderedBroadcast(i, PERM_PRIVATE, null, null, Activity.RESULT_OK, null, null)
    }

    private fun isNetworkAvailableAndConnected(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val isNetworkAvailable = cm.activeNetworkInfo != null
        val isNetworkConnected = isNetworkAvailable && cm.activeNetworkInfo.isConnected

        return isNetworkConnected
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "Service stop")
    }
}