package com.quangluu.demo.photogallery

import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import android.graphics.BitmapFactory


class FlickrFetcher {
    companion object {
        private val API_KEY = "4f721bbafa75bf6d2cb5af54f937bb70"
        val TAG = "FlickrFetchr"
        //val ENDPOINT = "https://api.flickr.com/services/rest/"
        val ENDPOINT = Uri.parse("https://api.flickr.com/services/rest/")
                .buildUpon()
                .appendQueryParameter("api_key", API_KEY)
                .appendQueryParameter("format", "json")
                .appendQueryParameter("nojsoncallback", "1")
                .appendQueryParameter("extras", "url_s")
                .build()
        private val FETCH_RECENT_METHOD = "flickr.photos.getRecent"
        private val SEARCH_METHOD = "flickr.photos.search"
    }

    @Throws(IOException::class)
    fun getUrlBytes(urlSpec: String): String? {
        val url = URL(urlSpec)
        val connection = url.openConnection() as HttpURLConnection

        try {
            if (connection.responseCode != HttpURLConnection.HTTP_OK)
                return null

            val stream = BufferedInputStream(connection.inputStream)
            val data: String = readStream(inputStream = stream)
            stream.close()
            return data
        } finally {
            connection.disconnect()
        }
    }

    fun getBitmapByURL(imageURL: String): Bitmap? {
        var bitmap: Bitmap? = null
        val input = java.net.URL(imageURL).openStream()
        // Decode Bitmap
        bitmap = BitmapFactory.decodeStream(input)
        return bitmap
    }

    private fun readStream(inputStream: BufferedInputStream): String {
        val bufferedReader = BufferedReader(InputStreamReader(inputStream))
        val stringBuilder = StringBuilder()
        bufferedReader.forEachLine { stringBuilder.append(it) }
        return stringBuilder.toString()
    }


    @Throws(IOException::class)
    fun getUrlString(urlSpec: String): String {
        return "${getUrlBytes(urlSpec)}"
    }

    fun buildUrl(method: String, query: String?): String {
        val uriBuilder = ENDPOINT.buildUpon()
                .appendQueryParameter("method", method)
        if (method.equals(SEARCH_METHOD)) {
            uriBuilder.appendQueryParameter("text", query)
        }
        return uriBuilder.build().toString()
    }

    fun fetchRecentPhotos(): ArrayList<GalleryItem> {
        val url = buildUrl(FETCH_RECENT_METHOD, null)
        return downloadGalleryItems(url)
    }

    fun searchPhotos(query: String): ArrayList<GalleryItem> {
        val url = buildUrl(SEARCH_METHOD, query)
        return downloadGalleryItems(url)
    }

    fun downloadGalleryItems(url: String): ArrayList<GalleryItem> {
        val items = ArrayList<GalleryItem>()
        try {
            val jsonString = getUrlString(url)
            Log.i(TAG, "Received json ${jsonString}")
            val jsonBody = JSONObject(jsonString)
            parseItem(items, jsonBody)
        } catch (ioe: IOException) {
            Log.i(TAG, "Failed to received json ${ioe}")
        } catch (je: JSONException) {
            Log.i(TAG, "Failed to parse json ${je}")
        }
        return items
    }

    @Throws(IOException::class, JSONException::class)
    private fun parseItem(items: ArrayList<GalleryItem>, jsonBody: JSONObject) {
        val photosJSONObject = jsonBody.getJSONObject("photos")
        val photoJSONArray = photosJSONObject.optJSONArray("photo")
        for (i in 0 until photoJSONArray.length()) {
            val photoJSONObject = photoJSONArray.getJSONObject(i)
            val galleryItem = GalleryItem(
                    photoJSONObject.getString("id"),
                    photoJSONObject.getString("title"))
            if (!photoJSONObject.has("url_s")) {
                continue
            }
            galleryItem.url = photoJSONObject.getString("url_s")
            items.add(galleryItem)
        }
    }
}
