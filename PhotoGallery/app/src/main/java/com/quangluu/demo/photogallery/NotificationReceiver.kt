package com.quangluu.demo.photogallery

import android.app.Activity
import android.app.Notification
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.app.NotificationManagerCompat
import android.util.Log

class NotificationReceiver : BroadcastReceiver() {
    companion object {
        const val TAG = "NotificationReceiver"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.i(TAG, "receiver result:${resultCode}")

        if (resultCode != Activity.RESULT_OK) {
            return
        }

        val requestCode = intent!!.getIntExtra(PollService.REQUEST_CODE, 0)
        val notification = intent!!.getParcelableExtra<Notification>(PollService.NOTIFICATION)

        val notificationManager = NotificationManagerCompat.from(context!!)
        notificationManager.notify(requestCode, notification)
    }
}