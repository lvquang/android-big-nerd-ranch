package com.quangluu.demo.photogallery

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v4.app.Fragment
import android.util.Log
import android.widget.Toast

abstract  class VisibleFragment: Fragment() {
    private val mOnShowNotification = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            Log.i(TAG, "cancelling notification")
            resultCode = Activity.RESULT_CANCELED
        }
    }

    companion object {
        val TAG = "VisibleFragment"
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(PollService.ACTION_SHOW_NOTIFICATION)
        activity!!.registerReceiver(mOnShowNotification, filter, PollService.PERM_PRIVATE, null)
    }

    override fun onStop() {
        super.onStop()
        activity!!.unregisterReceiver(mOnShowNotification)
    }
}