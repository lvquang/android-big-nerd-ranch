package com.quangluu.demo.photogallery

import android.graphics.Bitmap
import android.os.Handler
import android.os.HandlerThread
import android.os.Message
import android.util.Log
import java.io.IOException
import java.util.concurrent.ConcurrentHashMap

class ThumbnailDownloader<T>(val responseHandler: Handler) : HandlerThread(TAG) {
    private var mHasQuit = false
    private lateinit var mRequestHandler: Handler
    private val mRequestMap = ConcurrentHashMap<T, String>()
    private lateinit var mThumbnailDownloadListener: ThumbnailDownloadListener<T>

    interface ThumbnailDownloadListener<T> {
        fun onThumbnailDownload(target: T, thumbnail: Bitmap)
    }

    fun setThumbnailDownloadListener(listener: ThumbnailDownloadListener<T>) {
        mThumbnailDownloadListener = listener
    }

    companion object {
        const val TAG = "ThumbnailDownloader"
        const val MESSAGE_DOWNLOAD = 0
    }

    override fun quit(): Boolean {
        mHasQuit = true
        return super.quit()
    }

    fun queueThumbnail(target: T, url: String?) {
        Log.i(TAG, "Got a url: ${url}")
        if (url == null) {
            mRequestMap.remove(target)
        } else {
            mRequestMap.put(target, url)
            mRequestHandler.obtainMessage(MESSAGE_DOWNLOAD, target).sendToTarget()
        }
    }

    override fun onLooperPrepared() {
        super.onLooperPrepared()
        mRequestHandler = object : Handler() {
            override fun handleMessage(msg: Message?) {
                super.handleMessage(msg)
                if (msg?.what == MESSAGE_DOWNLOAD) {
                    val target = msg.obj as T
                    Log.i(TAG, "Got a request for URL: ${mRequestMap.get(target)}")
                    handleRequest(target)
                }
            }
        }
    }

    fun handleRequest(target: T) {
        try {
            val url: String? = mRequestMap.get(target)
            if (url == null)
                return
            val bitmap = FlickrFetcher().getBitmapByURL(url)
            Log.i(TAG, "Bitmap created")
            responseHandler.post(Runnable {
                if (mRequestMap.get(target)!= url || mHasQuit)
                    return@Runnable
                mRequestMap.remove(target)
                mThumbnailDownloadListener.onThumbnailDownload(target, bitmap!!)
            })
        } catch (ioe: IOException) {
            Log.e(TAG, "Error downloading image")
        }
    }

    fun clearQueue() {
        mRequestHandler.removeMessages(MESSAGE_DOWNLOAD)
        mRequestMap.clear()
    }
}