package com.quangluu.demo.photogallery

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.*
import android.widget.ImageView

class PhotoGalleryFragment : VisibleFragment() {
    private lateinit var mPhotoRecyclerView: RecyclerView
    var mItems = ArrayList<GalleryItem>()
    private lateinit var mThumbnailDownloader: ThumbnailDownloader<PhotoViewHolder>

    companion object {
        const val TAG = "PhotoGalleryFragment"

        fun newInstance(): PhotoGalleryFragment {
            return PhotoGalleryFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        setHasOptionsMenu(true)

        updateItem()

        val responseHandler = Handler()
        mThumbnailDownloader = ThumbnailDownloader(responseHandler)
        mThumbnailDownloader.setThumbnailDownloadListener(object : ThumbnailDownloader.ThumbnailDownloadListener<PhotoViewHolder> {
            override fun onThumbnailDownload(target: PhotoViewHolder, thumbnail: Bitmap) {
                val drawable = BitmapDrawable(resources, thumbnail)
                target.bindDrawable(drawable)
            }
        })
        mThumbnailDownloader.start()
        mThumbnailDownloader.looper
        Log.i(TAG, "Background thread started")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_photo_gallery, container, false)
        mPhotoRecyclerView = view.findViewById(R.id.photo_recycler_view)
        mPhotoRecyclerView.layoutManager = GridLayoutManager(activity!!, 3)
        setupAdapter()
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mThumbnailDownloader.clearQueue()
    }

    override fun onDestroy() {
        super.onDestroy()
        mThumbnailDownloader.quit()
        Log.i(TAG, "Background thread destroyed")
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.fragment_photo_gallery_menu, menu)
        val searchItem = menu?.findItem(R.id.menu_item_search)
        val searchView = searchItem?.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                Log.d(TAG, "On query text submit: ${query}")
                QueryPreferences.setStoredQuery(activity!!, query!!)
                updateItem()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                Log.d(TAG, "On query text submit: ${newText}")
                return false
            }
        })
        searchView.setOnSearchClickListener {
            val lastQuery = QueryPreferences.getStoredQuery(activity!!)
            searchView.setQuery(lastQuery, false)
        }
        val toggleItem = menu.findItem(R.id.menu_item_toggle_polling)
        if (PollService.isServiceAlarmOn(activity!!)) {
            toggleItem.setTitle(R.string.stop_polling)
        } else {
            toggleItem.setTitle(R.string.start_polling)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_item_clear -> {
                QueryPreferences.setStoredQuery(activity!!, null)
                updateItem()
                return true
            }
            R.id.menu_item_toggle_polling -> {
                val shouldStartAlarm = !PollService.isServiceAlarmOn(activity!!)
                PollService.setServiceAlarm(activity!!, shouldStartAlarm)
                activity!!.invalidateOptionsMenu()
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

    inner class FetchItemTask(val query: String?) : AsyncTask<Void, Void, ArrayList<GalleryItem>>() {
        override fun doInBackground(vararg params: Void?): ArrayList<GalleryItem> {
            if (query == null) {
                return FlickrFetcher().fetchRecentPhotos()
            } else {
                return FlickrFetcher().searchPhotos(query)
            }
        }

        override fun onPostExecute(result: ArrayList<GalleryItem>?) {
            mItems = if (result != null) result else mItems
            setupAdapter()
        }
    }

    inner class PhotoViewHolder(private val itemzView: View) : RecyclerView.ViewHolder(itemzView) {
        val mItemImageView = itemzView.findViewById<ImageView>(R.id.item_image_view)

        fun bindDrawable(drawable: Drawable) {
            mItemImageView.setImageDrawable(drawable)
        }
    }

    inner class PhotoAdapter(val galleryItems: List<GalleryItem>) : RecyclerView.Adapter<PhotoViewHolder>() {
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PhotoViewHolder {
            val inflater = LayoutInflater.from(activity!!)
            val view = inflater.inflate(R.layout.list_item_gallery, p0, false)
            return PhotoViewHolder(view)
        }

        override fun onBindViewHolder(p0: PhotoViewHolder, p1: Int) {
            val placeholder = resources.getDrawable(R.drawable.bill_up_close)
            p0.bindDrawable(placeholder)
            val galleryItem = galleryItems.get(p1)
            mThumbnailDownloader.queueThumbnail(p0, galleryItem.url)
        }

        override fun getItemCount(): Int {
            return galleryItems.size
        }
    }

    private fun setupAdapter() {
        if (isAdded) {
            mPhotoRecyclerView.adapter = PhotoAdapter(mItems)
        }
    }

    private fun updateItem() {
        val query = QueryPreferences.getStoredQuery(activity!!)

        FetchItemTask(query).execute()
    }

}