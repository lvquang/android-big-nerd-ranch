package com.quangluu.demo.photogallery

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment

class PhotoGalleryActivity : SingleFragmentActivity() {

    override fun createFragment(): Fragment {
        return PhotoGalleryFragment.newInstance()
    }
}
