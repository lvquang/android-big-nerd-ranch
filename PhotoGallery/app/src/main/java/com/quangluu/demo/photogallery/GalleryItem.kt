package com.quangluu.demo.photogallery

data class GalleryItem(
        val id: String,
        val caption: String,
        var url: String?
) {
    constructor(id: String, caption: String):this(id, caption, null)
}