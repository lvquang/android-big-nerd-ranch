package com.quangluu.demo.nerdlauncher

import android.content.Intent
import android.content.pm.ResolveInfo
import android.os.Bundle
import android.provider.LiveFolders.INTENT
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import java.util.*

class NerdLauncherFragment : Fragment() {
    private lateinit var recycleView: RecyclerView

    companion object {
        public const val TAG = "NerdLauncherFragment"

        fun newInstance(): NerdLauncherFragment {
            return NerdLauncherFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_nerd_launch, container, false)
        recycleView = view.findViewById(R.id.app_recycle_view)
        recycleView.layoutManager = LinearLayoutManager(activity)
        setupAdapter()
        return view
    }

    private fun setupAdapter() {
        val startupIntent = Intent(Intent.ACTION_MAIN)
        startupIntent.addCategory(Intent.CATEGORY_LAUNCHER)
        val pm = activity!!.packageManager
        val activities = pm.queryIntentActivities(startupIntent, 0)
        activities.sortWith(object : Comparator<ResolveInfo> {
            override fun compare(p0: ResolveInfo?, p1: ResolveInfo?): Int {
                val pm = activity!!.packageManager
                return String.CASE_INSENSITIVE_ORDER.compare(p0!!.loadLabel(pm).toString()
                        , p1!!.loadLabel(pm).toString())
            }
        })
        Log.d(TAG, "Found ${activities.size} activities")
        recycleView.adapter = ActivityAdapter(activities)
    }

    inner class ActivityHolder(private val itemzView: View) : RecyclerView.ViewHolder(itemzView) {
        private lateinit var mResolveInfo: ResolveInfo
        fun bindActivity(resolveInfo: ResolveInfo) {
            mResolveInfo = resolveInfo
            val pm = activity!!.packageManager
            val appName = mResolveInfo.loadLabel(pm).toString()
            (itemzView as TextView).text = appName
            (itemzView as TextView).setOnClickListener {
                val activityInfo = mResolveInfo.activityInfo
                val i = Intent(Intent.ACTION_MAIN).setClassName(activityInfo.applicationInfo.packageName, activityInfo.name)
                startActivity(i)
            }
        }
    }

    inner class ActivityAdapter(val activities: List<ResolveInfo>) : RecyclerView.Adapter<ActivityHolder>() {
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ActivityHolder {
            val layoutInflater = LayoutInflater.from(activity!!)
            val view = layoutInflater.inflate(android.R.layout.simple_list_item_1,p0,false)
            return ActivityHolder(view)
        }

        override fun onBindViewHolder(p0: ActivityHolder, p1: Int) {
            val resolveInfo = activities.get(p1)
            p0.bindActivity(resolveInfo)
        }

        override fun getItemCount(): Int {
            return activities.size
        }
    }
}